﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFirst.Models
{
    public class BlogPost
    {
        public int Id { get; set; }

        [Column(TypeName = "datetime2")] //0001.01.01 datetime = 1970.01.01
        public DateTime Date { get; set; }
        
        [MaxLength(200)]
        [Required]        
        public string Title { get; set; }

        [MaxLength(4000)]
        [MinLength(100)] //Entity Validation Errors
        [Required]
        public string Content { get; set; }

        public virtual Author Author { get; set; }
    }
}