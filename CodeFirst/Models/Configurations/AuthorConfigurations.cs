﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace CodeFirst.Models.Configurations
{
    public class AuthorConfigurations : EntityTypeConfiguration<Author>
    {
        public AuthorConfigurations()
        {
            this.HasKey(k => k.MyId);
            this.Property(p => p.MyId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Name).HasMaxLength(200).IsRequired();
        }
    }
}