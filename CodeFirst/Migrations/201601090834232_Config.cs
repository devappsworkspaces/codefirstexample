namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Config : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BlogPosts", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.BlogPosts", "Title", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.BlogPosts", "Content", c => c.String(nullable: false, maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BlogPosts", "Content", c => c.String());
            AlterColumn("dbo.BlogPosts", "Title", c => c.String());
            AlterColumn("dbo.BlogPosts", "Date", c => c.DateTime(nullable: false));
        }
    }
}
